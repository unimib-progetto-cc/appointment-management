package it.unimib.cc.dealer.bookings.utils;

public class BookingsConfigUtils {
	public final class CarsManagementMicroserviceEndpoint {

		private CarsManagementMicroserviceEndpoint() {}

		public static final String PREFIX = "endpoint.microservice.carsManagement.prefix";
		public static final String GET_CARS = "endpoint.microservice.carsManagement.getCars";
		public static final String GET_CAR_PREFIX = "endpoint.microservice.carsManagement.getCarPrefix";

	}
	
	public final class UsersManagementMicroserviceEndpoint {

		private UsersManagementMicroserviceEndpoint() {}

		public static final String PREFIX = "endpoint.microservice.usersManagement.prefix";
		public static final String GET_USERS = "endpoint.microservice.usersManagement.getUsers";
		public static final String GET_USER_PREFIX = "endpoint.microservice.usersManagement.getUserPrefix";

	}
}
