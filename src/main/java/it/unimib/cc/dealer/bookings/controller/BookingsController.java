package it.unimib.cc.dealer.bookings.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import it.unimib.cc.dealer.bookings.exception.BadRequestException;
import it.unimib.cc.dealer.bookings.model.Booking;
import it.unimib.cc.dealer.bookings.model.Bookings;
import it.unimib.cc.dealer.bookings.service.BookingService;
import it.unimib.cc.dealer.bookings.utils.BookingsConfigUtils.CarsManagementMicroserviceEndpoint;
import it.unimib.cc.dealer.bookings.utils.BookingsConfigUtils.UsersManagementMicroserviceEndpoint;
import it.unimib.cc.dealer.bookings.utils.SecurityUtils;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/bookings")
public class BookingsController {

	@Autowired
	private BookingService bookingService;

	@Autowired
	private Environment environment;

	@GetMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<?> getBooking(HttpServletRequest request, @PathVariable Long id) {
		SecurityUtils.checkParameters(request, new ArrayList<>());

		Booking booking = this.bookingService.findById(id);

		// Single entity response
		booking.add(WebMvcLinkBuilder.linkTo(BookingsController.class).slash(booking.getId()).withSelfRel());
		booking.add(Link
				.of(environment.getProperty(CarsManagementMicroserviceEndpoint.GET_CAR_PREFIX) + booking.getCarId())
				.withRel("car"));
		booking.add(Link
				.of(environment.getProperty(UsersManagementMicroserviceEndpoint.GET_USER_PREFIX) + booking.getClientId())
				.withRel("user"));
		
		return ResponseEntity.ok(booking);
	}

	@PutMapping(value = "/{id}", 
			produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, 
			consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<?> updateBooking(HttpServletRequest request, @PathVariable Long id,
			@RequestBody Booking newBooking) {
		SecurityUtils.checkParameters(request, new ArrayList<>());

		newBooking.setId(id);
		this.bookingService.update(newBooking);

		return ResponseEntity.noContent().build();
	}

	@DeleteMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<?> deleteBooking(HttpServletRequest request, @PathVariable Long id) {
		SecurityUtils.checkParameters(request, new ArrayList<>());

		if (Objects.isNull(id)) {
			throw new BadRequestException("Id is null");
		}

		Booking booking = this.bookingService.delete(id);

		return ResponseEntity.ok(booking);
	}

	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<?> getBookings(HttpServletRequest request, @RequestParam(required = false) Long clientId,
			@RequestParam(required = false) Long carId) {
		SecurityUtils.checkParameters(request, Arrays.asList(new String[] { "clientId", "carId" }));

		Bookings bookings;

		if (Objects.isNull(clientId) && Objects.isNull(carId)) {
			bookings = bookingService.findAll();
		} else if (Objects.isNull(clientId)) {
			bookings = bookingService.findByCarId(carId);
		} else if (Objects.isNull(carId)) {
			bookings = bookingService.findByClientId(clientId);
		} else {
			bookings = bookingService.findByClientAndCar(clientId, carId);
		}

		// Single entity response
		bookings.getBookings().forEach((booking) -> {
			booking.add(WebMvcLinkBuilder.linkTo(BookingsController.class).slash(booking.getId()).withSelfRel());
			booking.add(Link
					.of(environment.getProperty(CarsManagementMicroserviceEndpoint.GET_CAR_PREFIX) + booking.getCarId())
					.withRel("car"));
			booking.add(Link
					.of(environment.getProperty(UsersManagementMicroserviceEndpoint.GET_USER_PREFIX) + booking.getClientId())
					.withRel("user"));
		});

		// Global response level
		bookings.add(WebMvcLinkBuilder.linkTo(BookingsController.class).withSelfRel());
		bookings.add(Link.of(environment.getProperty(CarsManagementMicroserviceEndpoint.GET_CARS)).withRel("cars"));
		bookings.add(Link.of(environment.getProperty(UsersManagementMicroserviceEndpoint.GET_USERS)).withRel("users"));

		return new ResponseEntity<>(bookings, HttpStatus.OK);
	}

	@PostMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<?> saveBooking(HttpServletRequest request, @RequestBody Booking booking) {
		SecurityUtils.checkParameters(request, new ArrayList<>());
		
		Booking savedBooking = this.bookingService.save(booking);
		savedBooking.add(WebMvcLinkBuilder.linkTo(BookingsController.class).slash(savedBooking.getId()).withSelfRel());
		
		return new ResponseEntity<Booking>(savedBooking, HttpStatus.CREATED);
	}

	@GetMapping(value = "/between/{dateTime}", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<?> getBookingsSpecifiedDate(HttpServletRequest request,
			@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime dateTime) {
		SecurityUtils.checkParameters(request, new ArrayList<>());

		Bookings bookings = bookingService.findByDateBetween(dateTime);

		// Single entity response
		bookings.getBookings().forEach((booking) -> {
			booking.add(WebMvcLinkBuilder.linkTo(BookingsController.class).slash(booking.getId()).withSelfRel());
			booking.add(Link
					.of(environment.getProperty(CarsManagementMicroserviceEndpoint.GET_CAR_PREFIX) + booking.getCarId())
					.withRel("car"));
			booking.add(Link
					.of(environment.getProperty(UsersManagementMicroserviceEndpoint.GET_USER_PREFIX) + booking.getClientId())
					.withRel("user"));
		});

		// Global response level
		bookings.add(WebMvcLinkBuilder.linkTo(BookingsController.class).withSelfRel());
		bookings.add(Link.of(environment.getProperty(CarsManagementMicroserviceEndpoint.GET_CARS)).withRel("cars"));
		bookings.add(Link.of(environment.getProperty(UsersManagementMicroserviceEndpoint.GET_USERS)).withRel("users"));
		
		return new ResponseEntity<>(bookings, HttpStatus.OK);
	}

	@GetMapping(value = "/between", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<?> getBookingsBetweenDates(HttpServletRequest request,
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime from,
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime to) {
		SecurityUtils.checkParameters(request, Arrays.asList(new String[] { "from", "to" }));

		Bookings bookings = bookingService.findByInterval(from, to);

		// Single entity response
		bookings.getBookings().forEach((booking) -> {
			booking.add(WebMvcLinkBuilder.linkTo(BookingsController.class).slash(booking.getId()).withSelfRel());
			booking.add(Link
					.of(environment.getProperty(CarsManagementMicroserviceEndpoint.GET_CAR_PREFIX) + booking.getCarId())
					.withRel("car"));
			booking.add(Link
					.of(environment.getProperty(UsersManagementMicroserviceEndpoint.GET_USER_PREFIX) + booking.getClientId())
					.withRel("user"));
		});

		// Global response level
		bookings.add(WebMvcLinkBuilder.linkTo(BookingsController.class).withSelfRel());
		bookings.add(Link.of(environment.getProperty(CarsManagementMicroserviceEndpoint.GET_CARS)).withRel("cars"));
		bookings.add(Link.of(environment.getProperty(UsersManagementMicroserviceEndpoint.GET_USERS)).withRel("users"));
		
		return new ResponseEntity<>(bookings, HttpStatus.OK);
	}

	@GetMapping(value = "/start/{dateTime}", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<?> getBookingsByStartDateTime(HttpServletRequest request,
			@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime dateTime) {
		SecurityUtils.checkParameters(request, new ArrayList<>());

		Bookings bookings = bookingService.findByStartDateTime(dateTime);

		// Single entity response
		bookings.getBookings().forEach((booking) -> {
			booking.add(WebMvcLinkBuilder.linkTo(BookingsController.class).slash(booking.getId()).withSelfRel());
			booking.add(Link
					.of(environment.getProperty(CarsManagementMicroserviceEndpoint.GET_CAR_PREFIX) + booking.getCarId())
					.withRel("car"));
			booking.add(Link
					.of(environment.getProperty(UsersManagementMicroserviceEndpoint.GET_USER_PREFIX) + booking.getClientId())
					.withRel("user"));
		});

		// Global response level
		bookings.add(WebMvcLinkBuilder.linkTo(BookingsController.class).withSelfRel());
		bookings.add(Link.of(environment.getProperty(CarsManagementMicroserviceEndpoint.GET_CARS)).withRel("cars"));
		bookings.add(Link.of(environment.getProperty(UsersManagementMicroserviceEndpoint.GET_USERS)).withRel("users"));
		
		return new ResponseEntity<>(bookings, HttpStatus.OK);
	}

	@GetMapping(value = "/start", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<?> getBookingsByStartDateTimeBetween(HttpServletRequest request,
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime from,
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime to) {
		SecurityUtils.checkParameters(request, Arrays.asList(new String[] { "from", "to" }));

		Bookings bookings = bookingService.findByStartDateTimeBetween(from, to);

		// Single entity response
		bookings.getBookings().forEach((booking) -> {
			booking.add(WebMvcLinkBuilder.linkTo(BookingsController.class).slash(booking.getId()).withSelfRel());
			booking.add(Link
					.of(environment.getProperty(CarsManagementMicroserviceEndpoint.GET_CAR_PREFIX) + booking.getCarId())
					.withRel("car"));
			booking.add(Link
					.of(environment.getProperty(UsersManagementMicroserviceEndpoint.GET_USER_PREFIX) + booking.getClientId())
					.withRel("user"));
		});

		// Global response level
		bookings.add(WebMvcLinkBuilder.linkTo(BookingsController.class).withSelfRel());
		bookings.add(Link.of(environment.getProperty(CarsManagementMicroserviceEndpoint.GET_CARS)).withRel("cars"));
		bookings.add(Link.of(environment.getProperty(UsersManagementMicroserviceEndpoint.GET_USERS)).withRel("users"));

		return new ResponseEntity<>(bookings, HttpStatus.OK);
	}

	@GetMapping(value = "/end/{dateTime}", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<?> getBookingsByEndDateTime(HttpServletRequest request,
			@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime dateTime) {
		SecurityUtils.checkParameters(request, new ArrayList<>());

		Bookings bookings = bookingService.findByEndDateTime(dateTime);

		// Single entity response
		bookings.getBookings().forEach((booking) -> {
			booking.add(WebMvcLinkBuilder.linkTo(BookingsController.class).slash(booking.getId()).withSelfRel());
			booking.add(Link
					.of(environment.getProperty(CarsManagementMicroserviceEndpoint.GET_CAR_PREFIX) + booking.getCarId())
					.withRel("car"));
			booking.add(Link
					.of(environment.getProperty(UsersManagementMicroserviceEndpoint.GET_USER_PREFIX) + booking.getClientId())
					.withRel("user"));
		});

		// Global response level
		bookings.add(WebMvcLinkBuilder.linkTo(BookingsController.class).withSelfRel());
		bookings.add(Link.of(environment.getProperty(CarsManagementMicroserviceEndpoint.GET_CARS)).withRel("cars"));
		bookings.add(Link.of(environment.getProperty(UsersManagementMicroserviceEndpoint.GET_USERS)).withRel("users"));

		return new ResponseEntity<>(bookings, HttpStatus.OK);
	}

	@GetMapping(value = "/end", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<?> getBookingsByEndDateTimeBetween(HttpServletRequest request,
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime from,
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime to) {
		SecurityUtils.checkParameters(request, Arrays.asList(new String[] { "from", "to" }));

		Bookings bookings = bookingService.findByEndDateTimeBetween(from, to);

		// Single entity response
		bookings.getBookings().forEach((booking) -> {
			booking.add(WebMvcLinkBuilder.linkTo(BookingsController.class).slash(booking.getId()).withSelfRel());
			booking.add(Link
					.of(environment.getProperty(CarsManagementMicroserviceEndpoint.GET_CAR_PREFIX) + booking.getCarId())
					.withRel("car"));
			booking.add(Link
					.of(environment.getProperty(UsersManagementMicroserviceEndpoint.GET_USER_PREFIX) + booking.getClientId())
					.withRel("user"));
		});

		// Global response level
		bookings.add(WebMvcLinkBuilder.linkTo(BookingsController.class).withSelfRel());
		bookings.add(Link.of(environment.getProperty(CarsManagementMicroserviceEndpoint.GET_CARS)).withRel("cars"));
		bookings.add(Link.of(environment.getProperty(UsersManagementMicroserviceEndpoint.GET_USERS)).withRel("users"));

		return new ResponseEntity<>(bookings, HttpStatus.OK);
	}

}
