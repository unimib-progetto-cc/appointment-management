package it.unimib.cc.dealer.bookings;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class BookingManagementApp {

	public static void main(String[] args) {
		SpringApplication.run(BookingManagementApp.class, args);
	}
}