package it.unimib.cc.dealer.bookings.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@Entity
@Table(name = "booking")
@JacksonXmlRootElement(localName = "booking")
public class Booking extends RepresentationModel<Booking>{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "booking_id", unique = true, nullable = false)
	protected Long id;

	@Column(nullable = false)
	protected Long clientId;

	@Column(nullable = false)
	protected Long carId;

	@Column(nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	protected LocalDateTime startBookingDateTime;

	@Column(nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	protected LocalDateTime endBookingDateTime;

	// Default constructor.
	public Booking() { }

	public Booking(Long clientId, Long carId, LocalDateTime startDateTime, LocalDateTime endDateTime) {
		super();
		this.clientId = clientId;
		this.carId = carId;
		this.startBookingDateTime = startDateTime;
		this.endBookingDateTime = endDateTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public Long getCarId() {
		return carId;
	}

	public void setCarId(Long carId) {
		this.carId = carId;
	}

	public LocalDateTime getStartDateTime() {
		return startBookingDateTime;
	}

	public void setStartDateTime(LocalDateTime startDateTime) {
		this.startBookingDateTime = startDateTime;
	}

	public LocalDateTime getEndDateTime() {
		return endBookingDateTime;
	}

	public void setEndDateTime(LocalDateTime endDateTime) {
		this.endBookingDateTime = endDateTime;
	}

	@Override
	public String toString() {
		return "Booking [id=" + id + ", clientID: " + clientId + ", carID: " + carId + ", startDateTime: "
				+ startBookingDateTime + ", endDateTtime: " + endBookingDateTime + "]";
	}

}
