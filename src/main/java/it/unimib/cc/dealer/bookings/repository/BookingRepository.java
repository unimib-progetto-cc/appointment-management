package it.unimib.cc.dealer.bookings.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import it.unimib.cc.dealer.bookings.model.Booking;

public interface BookingRepository extends CrudRepository<Booking, Long> {

	List<Booking> findByClientIdAndCarId(Long clientId, Long carId);

	List<Booking> findByClientId(Long clientId);

	List<Booking> findByCarId(Long carId);

	List<Booking> findByStartBookingDateTime(LocalDateTime startDateTime);

	List<Booking> findByEndBookingDateTime(LocalDateTime endDateTime);

	List<Booking> findByStartBookingDateTimeBetween(LocalDateTime lowerDateTime, LocalDateTime upperDateTime);

	List<Booking> findByEndBookingDateTimeBetween(LocalDateTime lowerDateTime, LocalDateTime upperDateTime);

	List<Booking> findByStartBookingDateTimeLessThanEqualAndEndBookingDateTimeGreaterThanEqual(
			LocalDateTime dateTimeBetween, LocalDateTime dateTimeBetweenCopy);

	@Query(value = "SELECT * " + "FROM booking b " + "WHERE "
			+ "(b.start_booking_date_time between ?1 and ?2 OR b.end_booking_date_time between ?1 and ?2) " + "OR"
			+ "(b.start_booking_date_time <= ?1 AND b.end_booking_date_time >= ?2)", nativeQuery = true)
	List<Booking> findAllBookingsInPeriod(LocalDateTime startDateTime, LocalDateTime endDateTime);

}
