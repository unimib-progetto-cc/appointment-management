package it.unimib.cc.dealer.bookings.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unimib.cc.dealer.bookings.exception.BookingNotFoundException;
import it.unimib.cc.dealer.bookings.model.Booking;
import it.unimib.cc.dealer.bookings.model.Bookings;
import it.unimib.cc.dealer.bookings.repository.BookingRepository;

@Service
public class BookingService {

	@Autowired
	private BookingRepository bookingRepository;

	public Booking findById(Long id) {
		return bookingRepository.findById(id)
				.orElseThrow(() -> new BookingNotFoundException("Booking with id " + id + " not found"));
	}

	public Booking save(Booking booking) {
		return bookingRepository.save(booking);
	}

	public Booking update(Booking booking) {
		return bookingRepository.findById(booking.getId()).map(oldBooking -> {
			return bookingRepository.save(booking);
		}).orElseThrow(() -> new BookingNotFoundException("Booking with id " + booking.getId() + " not found"));
	}

	public Booking delete(Long id) {
		return bookingRepository.findById(id).map(car -> {
			bookingRepository.delete(car);
			return car;
		}).orElseThrow(() -> new BookingNotFoundException("Booking with id " + id + " not found"));
	}

	public Bookings findByClientAndCar(Long clientId, Long carId) {
		return new Bookings(bookingRepository.findByClientIdAndCarId(clientId, carId));
	}

	public Bookings findByClientId(Long clientId) {
		return new Bookings(bookingRepository.findByClientId(clientId));
	}

	public Bookings findByCarId(Long carId) {
		return new Bookings(bookingRepository.findByCarId(carId));
	}

	public Bookings findAll() {
		return new Bookings((List<Booking>) bookingRepository.findAll());
	}

	public Bookings findByDateBetween(LocalDateTime dateBetween) {
		return new Bookings(
				bookingRepository.findByStartBookingDateTimeLessThanEqualAndEndBookingDateTimeGreaterThanEqual(
						dateBetween, dateBetween));
	}

	public Bookings findByInterval(LocalDateTime startDateTime, LocalDateTime endDateTime) {
		return new Bookings(bookingRepository.findAllBookingsInPeriod(startDateTime, endDateTime));
	}

	public Bookings findByStartDateTime(LocalDateTime startDateTime) {
		return new Bookings(bookingRepository.findByStartBookingDateTime(startDateTime));
	}

	public Bookings findByStartDateTimeBetween(LocalDateTime lowerDateTime, LocalDateTime upperDateTime) {
		return new Bookings(bookingRepository.findByStartBookingDateTimeBetween(lowerDateTime, upperDateTime));
	}

	public Bookings findByEndDateTime(LocalDateTime endDateTime) {
		return new Bookings(bookingRepository.findByEndBookingDateTime(endDateTime));
	}

	public Bookings findByEndDateTimeBetween(LocalDateTime lowerDateTime, LocalDateTime upperDateTime) {
		return new Bookings(bookingRepository.findByEndBookingDateTimeBetween(lowerDateTime, upperDateTime));
	}

}
