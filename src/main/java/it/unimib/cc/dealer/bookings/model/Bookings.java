package it.unimib.cc.dealer.bookings.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "bookings")
public class Bookings extends RepresentationModel<Bookings> {

	@JacksonXmlProperty(localName = "booking")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Booking> bookings = new ArrayList<>();

	public Bookings(List<Booking> bookings) {
		super();
		this.bookings = bookings;
	}

	public List<Booking> getBookings() {
		return bookings;
	}

	public void setBookings(List<Booking> bookings) {
		this.bookings = bookings;
	}

	@Override
	public String toString() {
		return "Bookings [bookings=" + bookings + "]";
	}

}
