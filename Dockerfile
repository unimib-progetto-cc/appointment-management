# Build phase.
FROM maven:3.5-jdk-8 as build
COPY ./ /booking-management
WORKDIR /booking-management
RUN mvn clean package -Dmaven.test.skip=true

# Run phase.
FROM openjdk:8-jre-alpine
ARG VERSION
COPY --from=build /booking-management/target/bookings-$VERSION-SNAPSHOT.jar /app/bookings-app.jar
EXPOSE 8080
WORKDIR /app
ENTRYPOINT ["java", "-jar", "bookings-app.jar"]