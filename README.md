1) Run dockerfile to build app image: docker build --tag booking-app:latest .
2) Apply the PVC: kubectl apply -f mysql-pvc.yml
3) Apply the mysql Stateful Set: kubectl apply -f mysql-statefulset.yml
4) Apply the mysql Service: kubectl apply -f mysql-service.yml
5) Apply the booking app deployment obj: kubectl apply -f booking-deployment.yml
6) Apply the booking app Service: kubectl apply -f booking-service.yml